#Proyecto de sistema de Clinicas Dentales, implementado utilizando Scrum, arquitectura en capas, con sistema de comunicacion REST, y programado en Java.
* Programado por:
   - Camilo Andres D'isidoro,
   - Carlos Gonzales Paniagua,
   - Maria Esmeralda Paniagua Puente,
   - Pablo Valdes Balduz,
   - Gabriel Casado Valcarcel,
   - Feiyun Ye,
   - Jaime Benedi Galdeano,
#Desarrollado por estudiantes de la Facultad de Informatica de la Universidad Complutense de Madrid para la asignatura Ingenieria de Software 2 en el curso 2020-21
