package com.ucm.DentalManagement.services;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.ucm.DentalManagement.controller.ClientsResponse;
import com.ucm.DentalManagement.controller.NewClientsRequest;
import com.ucm.DentalManagement.controller.UniqueClientsResponse;
import com.ucm.DentalManagement.domain.ClientsToClientsResponseConverter;
import com.ucm.DentalManagement.domain.NewClientsRequestToClientsConverter;
import com.ucm.DentalManagement.model.Clients;
import com.ucm.DentalManagement.model.ClientsCrud;


import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class ClientsServicesImpl implements ClientsServices{

	private ClientsCrud repository;
	private ClientsToClientsResponseConverter entityToResponseConverter;
	private NewClientsRequestToClientsConverter requestToEntityConverter;
	
	@Override
	public ClientsResponse getAllClients() {
		ClientsResponse response = new ClientsResponse();
		List<Clients> allClients =repository.findAll();
		List<UniqueClientsResponse> uniqueClientsList = allClients.stream().map(e -> entityToResponseConverter.convert(e)).collect(Collectors.toList());
		response.setClients(uniqueClientsList);
		return response;
	}

	@Override
	public Optional<UniqueClientsResponse> getClient(String dni) {
		Optional<Clients> entity = repository.findByclientDni(dni);
		Optional<UniqueClientsResponse> response;
		response = entity.map(areaEntity -> entityToResponseConverter.convert(areaEntity));
		return response;
	}

	@Override
	public UniqueClientsResponse createClient(NewClientsRequest newClientRequest) {
		UniqueClientsResponse response;
		Optional <Clients> oldEntity = repository.findByclientDni(newClientRequest.getClientDni());
		if(oldEntity.isPresent()) {
			response = entityToResponseConverter.convert(oldEntity.get());
		} else {
			Clients newEntity =requestToEntityConverter.convert(newClientRequest);
			newEntity = repository.save(newEntity);
			response = entityToResponseConverter.convert(newEntity);
		}
		return response;
	}

}
