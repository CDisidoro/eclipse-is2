package com.ucm.DentalManagement.services;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.ucm.DentalManagement.controller.ClinicsResponse;
import com.ucm.DentalManagement.controller.NewClinicRequest;
import com.ucm.DentalManagement.controller.UniqueClinicsResponse;
import com.ucm.DentalManagement.domain.ClinicsToClinicsResponseConverter;
import com.ucm.DentalManagement.domain.NewClinicsRequestToClinicsConverter;
import com.ucm.DentalManagement.model.Clinics;
import com.ucm.DentalManagement.model.ClinicsCrud;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class ClinicsServicesImpl  implements ClinicsServices{

	private ClinicsCrud repository;
	private ClinicsToClinicsResponseConverter entityToResponseConverter;
	private NewClinicsRequestToClinicsConverter requestToEntityConverter;
	
	@Override
	public ClinicsResponse getAllClinics() {
		ClinicsResponse response = new ClinicsResponse();
		List<Clinics> allClinics = repository.findAll();
		List<UniqueClinicsResponse> uniqueClinicList = allClinics.stream().map(e -> entityToResponseConverter.convert(e)).collect(Collectors.toList());
		response.setClinics(uniqueClinicList);
		return response;
	}
	
	@Override
	public Optional<UniqueClinicsResponse> getClinic(String adress) {
		Optional<Clinics> entity = repository.findByclinicAdress(adress);
		Optional<UniqueClinicsResponse> response;
        response = entity.map(areaEntity -> entityToResponseConverter.convert(areaEntity));
		return response;
	}
	@Override
	public UniqueClinicsResponse createClinics(NewClinicRequest newClinicRequest) {
		UniqueClinicsResponse response;
		Optional<Clinics> oldEntity = repository.findById(newClinicRequest.getClinicAdress());
		if (oldEntity.isPresent()) {
            response = entityToResponseConverter.convert(oldEntity.get());
        } else {
        	Clinics newEntity = requestToEntityConverter.convert(newClinicRequest);
            newEntity = repository.save(newEntity);
            response = entityToResponseConverter.convert(newEntity);
        }
		return response;
	}
	
	
}

