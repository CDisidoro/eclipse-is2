package com.ucm.DentalManagement.services;

import java.util.Optional;

import com.ucm.DentalManagement.controller.ClinicsResponse;
import com.ucm.DentalManagement.controller.NewClinicRequest;
import com.ucm.DentalManagement.controller.UniqueClinicsResponse;


public interface ClinicsServices {
	ClinicsResponse getAllClinics();
	Optional<UniqueClinicsResponse> getClinic(String id);
	UniqueClinicsResponse createClinics(NewClinicRequest newClinicRequest);
}
