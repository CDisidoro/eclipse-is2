package com.ucm.DentalManagement.services;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.ucm.DentalManagement.controller.BillsResponse;
import com.ucm.DentalManagement.controller.NewBillsRequest;
import com.ucm.DentalManagement.controller.UniqueBillsResponse;
import com.ucm.DentalManagement.domain.BillsToBillsResponseConverter;
import com.ucm.DentalManagement.domain.NewBillsRequestToBillsConverter;
import com.ucm.DentalManagement.model.Bills;
import com.ucm.DentalManagement.model.BillsCrud;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class BillsServiceImpl implements BillsServices {
	
	private BillsCrud repository;
	private BillsToBillsResponseConverter entityToResponseConverter;
	private NewBillsRequestToBillsConverter requestToEntityConverter;

    @Override
	public BillsResponse getAllBills() {
    	BillsResponse response = new BillsResponse();
		List<Bills> allBills = repository.findAll();
		List<UniqueBillsResponse> uniqueBillsList = allBills.stream().map(e -> entityToResponseConverter.convert(e)).collect(Collectors.toList());
		response.setBills(uniqueBillsList);
		return response;
	}

	@Override
	public UniqueBillsResponse createBill(NewBillsRequest billsRequest) {
        UniqueBillsResponse response;
        Optional<Bills> oldEntity = repository.findByclientDni(billsRequest.getClientDni());
        if (oldEntity.isPresent()) {
            response = entityToResponseConverter.convert(oldEntity.get());
        } else {
        	Bills newEntity = requestToEntityConverter.convert(billsRequest);
            newEntity = repository.save(newEntity);
            response = entityToResponseConverter.convert(newEntity);
        }
		return response;
	}

	@Override
	public Optional<UniqueBillsResponse> getBill(String clientDni) {
		Optional<Bills> entity = repository.findByclientDni(clientDni);
		Optional<UniqueBillsResponse> response;
        response = entity.map(areaEntity -> entityToResponseConverter.convert(areaEntity));
		return response;
	}

}
