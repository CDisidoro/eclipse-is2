package com.ucm.DentalManagement.services;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.ucm.DentalManagement.controller.UsersResponse;
import com.ucm.DentalManagement.controller.NewUsersRequest;
import com.ucm.DentalManagement.controller.UniqueUsersResponse;
import com.ucm.DentalManagement.domain.UsersToUsersResponseConverter;
import com.ucm.DentalManagement.domain.NewUsersRequestToUsersConverter;
import com.ucm.DentalManagement.model.Users;
import com.ucm.DentalManagement.model.UsersCrud;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class UsersServicesImpl implements UsersServices {
	
	private UsersCrud repository;
	private UsersToUsersResponseConverter entityToResponseConverter;
	private NewUsersRequestToUsersConverter requestToEntityConverter;

    @Override
	public UsersResponse getAllUsers() {
    	UsersResponse response = new UsersResponse();
		List<Users> allUsers = repository.findAll();
		List<UniqueUsersResponse> uniqueUsersList = allUsers.stream().map(e -> entityToResponseConverter.convert(e)).collect(Collectors.toList());
		response.setUsers(uniqueUsersList);
		return response;
	}

	@Override
	public UniqueUsersResponse createUser(NewUsersRequest newUserRequest) {
        UniqueUsersResponse response;
        Optional<Users> oldEntity = repository.findByuserName(newUserRequest.getUserName());
        if (oldEntity.isPresent()) {
            response = entityToResponseConverter.convert(oldEntity.get());
        } else {
        	Users newEntity = requestToEntityConverter.convert(newUserRequest);
            newEntity = repository.save(newEntity);
            response = entityToResponseConverter.convert(newEntity);
        }
		return response;
	}

	@Override
	public Optional<UniqueUsersResponse> getUser(String userName) {
		Optional<Users> entity = repository.findByuserName(userName);
		Optional<UniqueUsersResponse> response;
        response = entity.map(areaEntity -> entityToResponseConverter.convert(areaEntity));
		return response;
	}

}
