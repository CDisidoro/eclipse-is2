package com.ucm.DentalManagement.services;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.ucm.DentalManagement.controller.NewProductsRequest;
import com.ucm.DentalManagement.controller.ProductsResponse;
import com.ucm.DentalManagement.controller.UniqueProductsResponse;
import com.ucm.DentalManagement.domain.NewProductsRequestToProductsConverter;
import com.ucm.DentalManagement.domain.ProductsToProductsResponseConverter;
import com.ucm.DentalManagement.model.Products;
import com.ucm.DentalManagement.model.ProductsCrud;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class ProductsServicesImpl implements ProductServices {

	private ProductsCrud repository;
	private ProductsToProductsResponseConverter entityToResponseConverter;
	private NewProductsRequestToProductsConverter requestToEntityConverter;

    @Override
	public ProductsResponse getAllProducts() {
    	ProductsResponse response = new ProductsResponse();
		List<Products> allProducts = repository.findAll();
		List<UniqueProductsResponse> uniqueProductsList = allProducts.stream().map(e -> entityToResponseConverter.convert(e)).collect(Collectors.toList());
		response.setProducts(uniqueProductsList);
		return response;
	}

	@Override
	public UniqueProductsResponse createProduct(NewProductsRequest productsRequest) {
        UniqueProductsResponse response;
        Optional<Products> oldEntity = repository.findByproductName(productsRequest.getProductName());
        if (oldEntity.isPresent()) {
            response = entityToResponseConverter.convert(oldEntity.get());
        } else {
        	Products newEntity = requestToEntityConverter.convert(productsRequest);
            newEntity = repository.save(newEntity);
            response = entityToResponseConverter.convert(newEntity);
        }
		return response;
	}

	@Override
	public Optional<UniqueProductsResponse> getProduct(String productName) {
		Optional<Products> entity = repository.findByproductName(productName);
		Optional<UniqueProductsResponse> response;
        response = entity.map(areaEntity -> entityToResponseConverter.convert(areaEntity));
		return response;
	}
	
}
