package com.ucm.DentalManagement.services;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.ucm.DentalManagement.controller.NewTreatmentsRequest;
import com.ucm.DentalManagement.controller.TreatmentsResponse;
import com.ucm.DentalManagement.controller.UniqueTreatmentsResponse;
import com.ucm.DentalManagement.domain.NewTreatmentsRequestToTreatmentsConverter;
import com.ucm.DentalManagement.domain.TreatmentsToTreatmentsResponseConverter;
import com.ucm.DentalManagement.model.Treatments;
import com.ucm.DentalManagement.model.TreatmentsCrud;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class TreatmentsServicesImpl implements TreatmentsServices {
	
	private TreatmentsCrud repository;
	private TreatmentsToTreatmentsResponseConverter entityToResponseConverter;
	private NewTreatmentsRequestToTreatmentsConverter requestToEntityConverter;

    @Override
	public TreatmentsResponse getAllTreatments() {
    	TreatmentsResponse response = new TreatmentsResponse();
		List<Treatments> allTreatments = repository.findAll();
		List<UniqueTreatmentsResponse> uniqueTreatmentsList = allTreatments.stream().map(e -> entityToResponseConverter.convert(e)).collect(Collectors.toList());
		response.setTreatments(uniqueTreatmentsList);
		return response;
	}

	@Override
	public UniqueTreatmentsResponse createTreatment(NewTreatmentsRequest treatmentsRequest) {
        UniqueTreatmentsResponse response;
        Optional<Treatments> oldEntity = repository.findBytreatmentDescription(treatmentsRequest.getTreatmentDescription());
        if (oldEntity.isPresent()) {
            response = entityToResponseConverter.convert(oldEntity.get());
        } else {
        	Treatments newEntity = requestToEntityConverter.convert(treatmentsRequest);
            newEntity = repository.save(newEntity);
            response = entityToResponseConverter.convert(newEntity);
        }
		return response;
	}

	@Override
	public Optional<UniqueTreatmentsResponse> getTreatment(String treatmentDescription) {
		Optional<Treatments> entity = repository.findBytreatmentDescription(treatmentDescription);
		Optional<UniqueTreatmentsResponse> response;
        response = entity.map(areaEntity -> entityToResponseConverter.convert(areaEntity));
		return response;
	}

}
