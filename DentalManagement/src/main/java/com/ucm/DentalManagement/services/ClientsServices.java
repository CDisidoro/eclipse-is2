package com.ucm.DentalManagement.services;

import java.util.Optional;
import com.ucm.DentalManagement.controller.ClientsResponse;
import com.ucm.DentalManagement.controller.NewClientsRequest;
import com.ucm.DentalManagement.controller.UniqueClientsResponse;

public interface ClientsServices {
	ClientsResponse getAllClients();
	Optional<UniqueClientsResponse> getClient(String dni);
	UniqueClientsResponse createClient(NewClientsRequest newClientRequest);
}
