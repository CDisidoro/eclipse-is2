package com.ucm.DentalManagement.services;

import java.util.Optional;

import com.ucm.DentalManagement.controller.NewProductsRequest;
import com.ucm.DentalManagement.controller.ProductsResponse;
import com.ucm.DentalManagement.controller.UniqueProductsResponse;


public interface ProductServices {
	ProductsResponse getAllProducts();
	Optional <UniqueProductsResponse> getProduct(String productName);
	UniqueProductsResponse createProduct(NewProductsRequest newProductsRequest);
}
