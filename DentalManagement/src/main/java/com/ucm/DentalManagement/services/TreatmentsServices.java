package com.ucm.DentalManagement.services;

import java.util.Optional;
import com.ucm.DentalManagement.controller.TreatmentsResponse;
import com.ucm.DentalManagement.controller.NewTreatmentsRequest;
import com.ucm.DentalManagement.controller.UniqueTreatmentsResponse;

public interface TreatmentsServices {
	TreatmentsResponse getAllTreatments();
	Optional<UniqueTreatmentsResponse> getTreatment(String treatmentDescription);
	UniqueTreatmentsResponse createTreatment(NewTreatmentsRequest newTreatmentRequest);
}
