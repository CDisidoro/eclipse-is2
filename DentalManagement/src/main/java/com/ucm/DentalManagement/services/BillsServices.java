package com.ucm.DentalManagement.services;

import java.util.Optional;

import com.ucm.DentalManagement.controller.NewBillsRequest;
import com.ucm.DentalManagement.controller.UniqueBillsResponse;
import com.ucm.DentalManagement.controller.BillsResponse;

public interface BillsServices {
	BillsResponse getAllBills();
	Optional<UniqueBillsResponse> getBill(String id);
	UniqueBillsResponse createBill(NewBillsRequest newBillRequest);
}
