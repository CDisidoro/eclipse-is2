package com.ucm.DentalManagement.services;

import java.util.Optional;
import com.ucm.DentalManagement.controller.NewUsersRequest;
import com.ucm.DentalManagement.controller.UniqueUsersResponse;
import com.ucm.DentalManagement.controller.UsersResponse;

public interface UsersServices {
	UsersResponse getAllUsers();
	Optional<UniqueUsersResponse> getUser(String userName);
	UniqueUsersResponse createUser(NewUsersRequest newUserRequest);
}
