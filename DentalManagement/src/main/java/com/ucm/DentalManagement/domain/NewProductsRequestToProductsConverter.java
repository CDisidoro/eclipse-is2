package com.ucm.DentalManagement.domain;

import org.springframework.core.convert.converter.Converter;

import org.springframework.stereotype.Component;

import com.ucm.DentalManagement.controller.NewProductsRequest;
import com.ucm.DentalManagement.model.Products;

@Component
public class NewProductsRequestToProductsConverter implements Converter<NewProductsRequest, Products>{

	@Override
	public Products convert(NewProductsRequest source) {
		Products entity = new Products();
		entity.setDay_of_arrival(source.getDay_of_arrival());
		entity.setProduct_id(source.getProduct_id());
		entity.setProductName(source.getProductName());
		entity.setStock_num(source.getStock_num());
		entity.setStore_id(source.getStore_id());
		return entity;
	}

}
