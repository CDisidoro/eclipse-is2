package com.ucm.DentalManagement.domain;

import org.springframework.core.convert.converter.Converter;

import org.springframework.stereotype.Component;

import com.ucm.DentalManagement.controller.UniqueProductsResponse;
import com.ucm.DentalManagement.model.Products;

@Component
public class ProductsToProductsResponseConverter implements Converter<Products, UniqueProductsResponse> {


	@Override
	public UniqueProductsResponse convert(Products source) {
		UniqueProductsResponse response = new UniqueProductsResponse();
		response.setDay_of_arrival(source.getDay_of_arrival());
		response.setProduct_id(source.getProduct_id());
		response.setProductName(source.getProductName());
		response.setStock_num(source.getStock_num());
		response.setStore_id(source.getStore_id());
		
		return response;
	}

}