package com.ucm.DentalManagement.domain;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UsersResource {

	private int user_id;
	private String userName;
	private String user_email;
	private String user_password;
	private int user_telephone;
	private String user_type;
	private int user_available;
	
}
