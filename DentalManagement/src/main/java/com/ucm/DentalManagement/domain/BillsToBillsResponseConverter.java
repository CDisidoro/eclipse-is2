package com.ucm.DentalManagement.domain;

import org.springframework.core.convert.converter.Converter;

import org.springframework.stereotype.Component;

import com.ucm.DentalManagement.controller.UniqueBillsResponse;
import com.ucm.DentalManagement.model.Bills;

@Component
public class BillsToBillsResponseConverter implements Converter<Bills, UniqueBillsResponse> {

	@Override
	public UniqueBillsResponse convert(Bills source) {
		UniqueBillsResponse response = new UniqueBillsResponse();
		response.setBill_payed(source.getBill_payed());
		response.setBill_price(source.getBill_price());
		response.setClientDni(source.get_client_dni());
		response.setBill_expedition_date(source.getBill_expedition_date());
		return response;
	}

}