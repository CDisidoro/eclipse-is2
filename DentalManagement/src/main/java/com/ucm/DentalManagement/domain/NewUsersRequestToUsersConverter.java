package com.ucm.DentalManagement.domain;

import org.springframework.core.convert.converter.Converter;

import org.springframework.stereotype.Component;

import com.ucm.DentalManagement.controller.NewUsersRequest;
import com.ucm.DentalManagement.model.Users;

@Component
public class NewUsersRequestToUsersConverter implements Converter<NewUsersRequest, Users>{

	@Override
	public Users convert(NewUsersRequest source) {
		Users entity = new Users();
		entity.setUserName(source.getUserName());
		entity.setUser_email(source.getUser_email());
		entity.setUser_password(source.getUser_password());
		entity.setUser_telephone(source.getUser_telephone());
		entity.setUser_type(source.getUser_type());
		entity.setUser_available(source.getUser_available());
		return entity;
	}

}
