package com.ucm.DentalManagement.domain;

import org.springframework.core.convert.converter.Converter;

import org.springframework.stereotype.Component;

import com.ucm.DentalManagement.controller.NewClientsRequest;
import com.ucm.DentalManagement.model.Clients; 

@Component
public class NewClientsRequestToClientsConverter implements Converter<NewClientsRequest, Clients>{

	@Override
	public Clients convert(NewClientsRequest source) {
		Clients entity = new Clients();
		entity.setClientDni(source.getClientDni());
		entity.setClient_name(source.getClient_name());
		entity.setClient_birthdate(source.getClient_birthdate());
		entity.setClient_address(source.getClient_address());
		return entity;
	}
}
