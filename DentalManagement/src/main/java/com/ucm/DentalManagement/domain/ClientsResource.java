package com.ucm.DentalManagement.domain;

import java.sql.Date;

import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClientsResource {
	
	private String clientDni;
	private String client_name;
	private Date client_birthdate;
	private String client_address;
}
