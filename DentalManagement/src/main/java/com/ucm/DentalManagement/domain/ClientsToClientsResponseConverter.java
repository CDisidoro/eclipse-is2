package com.ucm.DentalManagement.domain;

import org.springframework.core.convert.converter.Converter;

import org.springframework.stereotype.Component;

import com.ucm.DentalManagement.controller.UniqueClientsResponse;
import com.ucm.DentalManagement.model.Clients;

@Component
public class ClientsToClientsResponseConverter implements Converter<Clients, UniqueClientsResponse>{

	@Override
	public UniqueClientsResponse convert(Clients source) {
		UniqueClientsResponse response = new UniqueClientsResponse();
		response.setClientDni(source.getClientDni());
		response.setClient_name(source.getClient_name());
		response.setClient_birthdate(source.getClient_birthdate());
		response.setClient_address(source.getClient_address());
		return response;
	}

}
