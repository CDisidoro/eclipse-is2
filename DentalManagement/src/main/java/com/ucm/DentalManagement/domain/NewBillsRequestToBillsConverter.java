package com.ucm.DentalManagement.domain;

import org.springframework.core.convert.converter.Converter;

import org.springframework.stereotype.Component;

import com.ucm.DentalManagement.controller.NewBillsRequest;
import com.ucm.DentalManagement.model.Bills;

@Component
public class NewBillsRequestToBillsConverter implements Converter<NewBillsRequest, Bills>{

	@Override
	public Bills convert(NewBillsRequest source) {
		Bills entity = new Bills();
		entity.setBill_payed(source.getBill_payed());
		entity.setBill_price(source.getBill_price());
		entity.set_client_dni(source.getClientDni());
		entity.setBill_expedition_date(source.getBill_expedition_date());
		return entity;
	}

}