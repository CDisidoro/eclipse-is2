package com.ucm.DentalManagement.domain;

import org.springframework.core.convert.converter.Converter;

import org.springframework.stereotype.Component;

import com.ucm.DentalManagement.controller.UniqueUsersResponse;
import com.ucm.DentalManagement.model.Users;

@Component
public class UsersToUsersResponseConverter implements Converter<Users, UniqueUsersResponse> {

	@Override
	public UniqueUsersResponse convert(Users source) {
		UniqueUsersResponse response = new UniqueUsersResponse();
		response.setUserName(source.getUserName());
		response.setUser_email(source.getUser_email());
		response.setUser_password(source.getUser_password());
		response.setUser_telephone(source.getUser_telephone());
		response.setUser_type(source.getUser_type());
		response.setUser_available(source.getUser_available());
		return response;
	}

}