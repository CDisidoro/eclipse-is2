package com.ucm.DentalManagement.domain;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BillsResource {
	
	private int bill_id;
	private String bill_payed;
	private int bill_price;
	private String clientDni;
	private Date bill_expedition_date;

}
