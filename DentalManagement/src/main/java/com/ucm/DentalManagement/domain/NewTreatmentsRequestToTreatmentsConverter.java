package com.ucm.DentalManagement.domain;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ucm.DentalManagement.controller.NewTreatmentsRequest;
import com.ucm.DentalManagement.model.Treatments;

@Component
public class NewTreatmentsRequestToTreatmentsConverter implements Converter<NewTreatmentsRequest, Treatments> {

	@Override
	public Treatments convert(NewTreatmentsRequest source) {
		Treatments entity = new Treatments();
		entity.setClient_dni(source.getClient_dni());
		entity.setTreatment_end_date(source.getTreatment_end_date());
		entity.setTreatment_price(source.getTreatment_price());
		entity.setTreatment_start_date(source.getTreatment_start_date());
		entity.setTreatmentDescription(source.getTreatmentDescription());
		
		return entity;
	}
	
}
