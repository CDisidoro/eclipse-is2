package com.ucm.DentalManagement.domain;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ucm.DentalManagement.controller.UniqueClinicsResponse;
import com.ucm.DentalManagement.model.Clinics;

@Component
public class ClinicsToClinicsResponseConverter implements Converter<Clinics, UniqueClinicsResponse>{

	@Override
	public UniqueClinicsResponse convert(Clinics source) {
		UniqueClinicsResponse response = new UniqueClinicsResponse();
		response.setClinicAdress(source.getClinicAdress());
		return response;
	}

}
