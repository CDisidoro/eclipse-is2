package com.ucm.DentalManagement.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClinicsResource {
	private int clinic_id;
	private String clinicAdress;
}
