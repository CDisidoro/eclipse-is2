package com.ucm.DentalManagement.domain;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ucm.DentalManagement.controller.NewClinicRequest;
import com.ucm.DentalManagement.model.Clinics;

@Component
public class NewClinicsRequestToClinicsConverter implements Converter<NewClinicRequest, Clinics>{

	@Override
	public Clinics convert(NewClinicRequest source) {
		Clinics entity = new Clinics();
		entity.setClinicAdress(source.getClinicAdress());
		return entity;
	}
}