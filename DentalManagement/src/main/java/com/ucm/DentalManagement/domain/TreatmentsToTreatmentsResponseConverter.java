package com.ucm.DentalManagement.domain;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ucm.DentalManagement.controller.UniqueTreatmentsResponse;
import com.ucm.DentalManagement.model.Treatments;

@Component
public class TreatmentsToTreatmentsResponseConverter implements Converter<Treatments, UniqueTreatmentsResponse>{

	@Override
	public UniqueTreatmentsResponse convert(Treatments source) {
		UniqueTreatmentsResponse response = new UniqueTreatmentsResponse();
		response.setClient_dni(source.get_client_dni());
		response.setTreatment_end_date(source.get_treatment_end_date());
		response.setTreatment_price(source.get_treatment_price());
		response.setTreatment_start_date(source.getTreatment_start_date());
		response.setTreatmentDescription(source.getTreatmentDescription());
		
		return response;
	}

}
