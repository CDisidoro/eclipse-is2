package com.ucm.DentalManagement.domain;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductsResource {
	
	private int store_id;
	private String productName;
	private Date day_of_arrival;
	private Integer stock_num;

}
