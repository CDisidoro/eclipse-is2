package com.ucm.DentalManagement.domain;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TreatmentsResource {

	private int treatment_id;
	private String client_dni;
	private String treatmentDescription;
	private int treatment_price;
	private Date treatment_start_date;
	private Date treatment_end_date;
}
