package com.ucm.DentalManagement.model;

import java.sql.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "BILLS")
public class Bills {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int bill_id;
	//@NotEmpty
	private String clientDni;
	//@NotEmpty
	private int bill_price;
	//@NotEmpty
	private Date bill_expedition_date;
	//@NotEmpty
	private String bill_payed;
	
	public Bills() {
		super();
	}
	public Bills(int bill_id, String client_dni, int bill_price, Date bill_expedition_date, String bill_payed) {
		this.bill_id = bill_id;
		this.clientDni = client_dni;
		this.bill_price = bill_price;
		this.bill_expedition_date = bill_expedition_date;
		this.bill_payed = bill_payed;
	}
	
	public int get_bill_id() {
		return bill_id;
	}
	public String get_client_dni() {
		return clientDni;
	}
	public String get_bill_payed() {
		return bill_payed;
	}
	public int get_bill_price() {
		return bill_price;
	}
	public Date get_bill_expedition_date() {
		return bill_expedition_date;
	}
	public void set_bill_id(int bill_id) {
		this.bill_id = bill_id;
	}
	public void set_client_dni(String client_dni) {
		this.clientDni = client_dni;
	}
	public void set_bill_payed(String bill_payed) {
		this.bill_payed = bill_payed;
	}
	public void set_bill_price(int bill_price) {
		this.bill_price = bill_price;
	}
	public void set_bill_expedition_date(Date bill_expedition_date) {
		this.bill_expedition_date = bill_expedition_date;
	}
}
