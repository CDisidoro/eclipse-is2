package com.ucm.DentalManagement.model;


import javax.persistence.*;
import javax.validation.constraints.*;
import lombok.Data;

@Data
@Entity
@Table(name = "CLINICS")
public class Clinics {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int store_id;
	@NotEmpty
	private String clinicAdress;
	public Clinics() {
		super();
	}
	public Clinics(int store_id, String adress) {
		this.clinicAdress = adress;
		this.store_id = store_id;
	}
	public String get_clinic_adress() {
		return clinicAdress;
	}
	public int get_store_id() {
		return store_id;
	}
	public void set_clinic_adress(String clinic_adress) {
		this.clinicAdress = clinic_adress;
	}
	public void set_store_id(int store_id) {
		this.store_id = store_id;
	}
}

