package com.ucm.DentalManagement.model;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TreatmentsCrud extends CrudRepository<Treatments, Integer>{
	List<Treatments> findAll();
	
	Optional<Treatments> findBytreatmentDescription(String treatmentDescription);
}
