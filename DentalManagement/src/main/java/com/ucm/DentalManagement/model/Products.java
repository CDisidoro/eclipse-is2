package com.ucm.DentalManagement.model;

import java.sql.Date;
import javax.persistence.*;
import javax.validation.constraints.*;
import lombok.Data;


@Data
@Entity
@Table(name = "PRODUCTS")
public class Products {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int product_id;
	//@NotEmpty
	private int store_id;
	//@NotEmpty
	private String productName;
	//@NotEmpty
	private Date day_of_arrival;
	//@NotEmpty
	private Integer stock_num;
}


