package com.ucm.DentalManagement.model;

import java.sql.Date;
import javax.persistence.*;
import javax.validation.constraints.*;
import lombok.Data;

@Data
@Entity
@Table(name = "CLIENTS")
public class Clients {
	@Id
	@NotEmpty
	private String clientDni;
	@NotEmpty
	private String client_name;
	@NotEmpty
	private Date client_birthdate;
	@NotEmpty
	private String client_address;
	public Clients() {
		super();
	}
	public Clients(String client_name, String client_dni, Date client_birthdate, String client_address) {
		this.client_name = client_name;
		this.clientDni = client_dni;
		this.client_birthdate = client_birthdate;
		this.client_address = client_address;
	}
	public String get_client_dni() {
		return clientDni;
	}
	public String get_client_name() {
		return client_name;
	}
	public String get_client_address() {
		return client_address;
	}
	public Date get_client_birthdate() {
		return client_birthdate;
	}
	public void set_client_dni(String client_dni) {
		this.clientDni = client_dni;
	}
	public void set_client_name(String client_name) {
		this.client_name = client_name;
	}
	public void set_client_address(String client_address) {
		this.client_address = client_address;
	}
	public void set_client_birthdate(Date client_birthdate) {
		this.client_birthdate = client_birthdate;
	}
}
