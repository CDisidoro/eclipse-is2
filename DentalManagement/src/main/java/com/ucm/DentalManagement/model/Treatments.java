package com.ucm.DentalManagement.model;

import java.sql.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import lombok.Data;

@Data
@Entity
@Table(name = "TREATMENTS")
public class Treatments {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int treatment_id;
	//@NotEmpty
	private String client_dni;
	//@NotEmpty
	private String treatmentDescription;
	//@NotEmpty
	private int treatment_price;
	//@NotEmpty
	private Date treatment_start_date;
	//@NotEmpty
	private Date treatment_end_date;
	public Treatments() {
		super();
	}
	public Treatments(int treatment_id, String client_dni, String treatment_description, int treatment_price, Date treatment_start_date, Date treatment_end_date) {
		this.treatment_id = treatment_id;
		this.client_dni = client_dni;
		this.treatmentDescription = treatment_description;
		this.treatment_price = treatment_price;
		this.treatment_start_date = treatment_start_date;
		this.treatment_end_date = treatment_end_date;
	}
	public int get_treatment_id() {
		return treatment_id;
	}
	public String get_client_dni() {
		return client_dni;
	}
	public String get_treatment_description() {
		return treatmentDescription;
	}
	public int get_treatment_price() {
		return treatment_price;
	}
	public Date get_treatment_start_date() {
		return treatment_start_date;
	}
	public Date get_treatment_end_date() {
		return treatment_end_date;
	}
	public void set_treatment_id(int treatment_id) {
		this.treatment_id = treatment_id;
	}
	public void set_client_dni(String client_dni) {
		this.client_dni = client_dni;
	}
	public void set_treatment_description(String treatment_description) {
		this.treatmentDescription = treatment_description;
	}
	public void set_treatment_price(int treatment_price) {
		this.treatment_price = treatment_price;
	}
	public void set_treatment_start_date(Date treatment_start_date) {
		this.treatment_start_date = treatment_start_date;
	}
	public void set_treatment_end_date(Date treatment_end_date) {
		this.treatment_end_date = treatment_end_date;
	}
}
