package com.ucm.DentalManagement.model;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersCrud extends JpaRepository<Users, Long>{

	Optional<Users> findByuserName(final String userName);
	
	@Query("SELECT u FROM Users u WHERE u.userName = ?1")
	public Users findWithUserName(String userName);
	
	List<Users> findAll();
}
