package com.ucm.DentalManagement.model;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClinicsCrud extends CrudRepository<Clinics, String> {
	List<Clinics> findAll();
	
	Optional<Clinics> findByclinicAdress(String adress);
}
