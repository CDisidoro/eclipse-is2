package com.ucm.DentalManagement.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.*;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.Data;

@Data
@Entity
@Table(name = "USERS")
public class Users {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int user_id;
	@NotEmpty
	private String userName;
	@NotEmpty
	@Email
	private String user_email;
	@NotEmpty
	private String user_password;
	//@NotEmpty
	private int user_telephone;
	@NotEmpty
	private String user_type;
	//@NotEmpty
	private int user_available;
	public Users() {
		super();
	}
	public Users(int user_id, String user_name, String user_email, String user_password, int user_telephone, String user_type, int user_available) {
		this.user_id = user_id;
		this.userName = user_name;
		this.user_email = user_email;
		this.user_password = user_password;
		this.user_telephone = user_telephone;
		this.user_type = user_type;
		this.user_available = user_available;
	}
	//Getters
	public int getUserId() {
		return user_id;
	}
	public String getUserName() {
		return userName;
	}
	public String getUserEmail() {
		return user_email;
	}
	public String getUserPassword() {
		return user_password;
	}
	public int getUserTelephone() {
		return user_telephone;
	}
	public String getUserType() {
		return user_type;
	}
	public int getUserAvailable() {
		return user_available;
	}
	//Setters
	public void setUserId(int user_id) {
		this.user_id = user_id;
	}
	public void setUserName(String user_name) {
		this.userName = user_name;
	}
	public void setUserEmail(String user_email) {
		this.user_email = user_email;
	}
	public void setUserPassword(String user_password) {
		this.user_password = user_password;
	}
	public void setUserTelephone(int user_telephone) {
		this.user_telephone = user_telephone;
	}
	public void setUserType(String user_type) {
		this.user_type = user_type;
	}
	public void setUserAvailable(int user_available) {
		this.user_available = user_available;
	}
}
