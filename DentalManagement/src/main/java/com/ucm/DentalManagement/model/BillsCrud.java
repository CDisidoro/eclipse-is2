package com.ucm.DentalManagement.model;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BillsCrud extends CrudRepository<Bills, Integer>{
	List<Bills> findAll();
	Optional<Bills> findByclientDni(String clientDni);
}
