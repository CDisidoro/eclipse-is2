package com.ucm.DentalManagement.model;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientsCrud extends CrudRepository<Clients, String>{
	
	Optional <Clients> findByclientDni(final String dni);
	
	//@Query("SELECT u FROM Clients u WHERE u.client_dni = ?1")
	//public Clients findWithDni(String dni);
	
	List<Clients> findAll();
}
