
package com.ucm.DentalManagement.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;


import com.ucm.DentalManagement.services.ClientsServicesImpl;

import lombok.AllArgsConstructor;

import com.ucm.DentalManagement.model.Clients;

import java.net.URI;

import javax.validation.Valid;

@RestController
@AllArgsConstructor
public class ClientsCrudController {
	@Autowired

	private ClientsServicesImpl service;
	
	@RequestMapping(path="/listClients", method= RequestMethod.GET)
	public ResponseEntity<ClientsResponse> getAvailableClients(){
		return ResponseEntity.ok(service.getAllClients());
	}
	
	@RequestMapping(path="/newClient", method = RequestMethod.GET)
	public String newClient(ModelMap map) {
		map.put("client", new Clients());
		return "newClient";
	}
	
	@RequestMapping(path="/createClient", method = RequestMethod.POST)
	public ResponseEntity<UniqueClientsResponse> createClient(@Valid @RequestBody NewClientsRequest clientRequest){
        UniqueClientsResponse resource = service.createClient(clientRequest);
        URI uriCreated = UriComponentsBuilder.fromPath("/" + "client-dni").buildAndExpand(clientRequest.getClientDni()).toUri();
        return ResponseEntity.created(uriCreated).body(resource);
	}
	
	@RequestMapping(path="/clientCreated", method = RequestMethod.POST)
	public String clientCreated(@RequestParam("client") Clients client) {
		return "clientCreated";
	}
	
	/*@RequestMapping(path = "/listClients", method = RequestMethod.GET)
	public String getAvailableClients(Model map) {
		System.out.println("Obteniendo todos los clientes disponibles...");
		map.addAttribute("clients", clientCrud.findAll());
		return "clientsList";
	}
	
	@RequestMapping(path="/newClient", method = RequestMethod.GET)
	public String newClient(ModelMap map) {
		map.put("client", new Clients());
		return "newClient";
	}
	
	@RequestMapping(path="/createClient", method = RequestMethod.POST)
	public String createClient(@Valid Clients client, BindingResult bindingResult, ModelMap map) {
		if(bindingResult.hasErrors()) {
			return "redirect:/newClient";
		}else {
			clientCrud.save(client);
			map.put("client",client);
			return "redirect:/ClientCreated";
		}
	}
	
	@RequestMapping(path="/clientCreated", method = RequestMethod.POST)
	public String clientCreated(@RequestParam("client") Clients client) {
		return "clientCreated";
	}*/
}

