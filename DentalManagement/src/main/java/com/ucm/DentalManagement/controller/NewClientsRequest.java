package com.ucm.DentalManagement.controller;

import java.sql.Date;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class NewClientsRequest {
	@NotNull
	private String clientDni;
	@NotNull
	private String client_name;
	@NotNull
	private Date client_birthdate;
	@NotNull
	private String client_address;
}
