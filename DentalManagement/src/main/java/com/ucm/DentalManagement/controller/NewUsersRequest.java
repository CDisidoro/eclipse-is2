package com.ucm.DentalManagement.controller;

import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class NewUsersRequest {
	@NotNull
	private int user_id;
	@NotNull
	private String userName;
	
	@NotNull
	private String user_email;
	
	@NotNull
	private String user_password;
	
	@NotNull
	private int user_telephone;
	
	@NotNull
	private String user_type;
	
	@NotNull
	private int user_available;	

}
