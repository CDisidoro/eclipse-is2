package com.ucm.DentalManagement.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriComponentsBuilder;

import com.ucm.DentalManagement.services.BillsServices;
import com.ucm.DentalManagement.model.Bills;

import java.net.URI;

import javax.validation.Valid;

@Controller
public class BillsCrudController {
	@Autowired
	private BillsServices service;
	//private BillsCrud billCrud;
	
	@RequestMapping(path="/listBills", method= RequestMethod.GET)
	public ResponseEntity<BillsResponse> getAvailableBills(){
		return ResponseEntity.ok(service.getAllBills());
	}
	
	@RequestMapping(path="/newBill", method = RequestMethod.GET)
	public String newBill(ModelMap map) {
		map.put("bill", new Bills());
		return "newBill";
	}
	
	@RequestMapping(path="/createBill", method = RequestMethod.POST)
	public ResponseEntity<UniqueBillsResponse> createBill(@Valid @RequestBody NewBillsRequest billRequest){
        UniqueBillsResponse resource = service.createBill(billRequest);
        URI uriCreated = UriComponentsBuilder.fromPath("/" + "user-id").buildAndExpand(billRequest.getBill_id()).toUri();
        return ResponseEntity.created(uriCreated).body(resource);
	}
	
	@RequestMapping(path="/billCreated", method = RequestMethod.POST)
	public String billCreated(@RequestParam("bill") Bills bill) {
		return "billCreated";
	}
	
	/*@RequestMapping(path = "/listBills", method = RequestMethod.GET)
	public String getAvailableBills(Model map) {
		System.out.println("Obteniendo todos los recibos disponibles...");
		map.addAttribute("bills", billCrud.findAll());
		return "billsList";
	}
	@RequestMapping(path="/newBill", method = RequestMethod.GET)
	public String newBill(ModelMap map) {
		map.put("bill", new Bills());
		return "newBill";
	}
	@RequestMapping(path="/createBill", method = RequestMethod.POST)
	public String createBill(@Valid Bills bill, BindingResult bindingResult, ModelMap map) {
		if(bindingResult.hasErrors()) {
			return "redirect:/newBill";
		}else {
			billCrud.save(bill);
			map.put("bill",bill);
			return "redirect:/billCreated";
		}
	}
	@RequestMapping(path="/billCreated", method = RequestMethod.POST)
	public String billCreated(@RequestParam("bill") Bills bill) {
		return "billCreated";
	}*/
}
