package com.ucm.DentalManagement.controller;

import java.sql.Date;
import lombok.Data;

@Data
public class UniqueClientsResponse {
	private String clientDni;
	private String client_name;
	private Date client_birthdate;
	private String client_address;
}
