package com.ucm.DentalManagement.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.ucm.DentalManagement.services.TreatmentsServices;

import lombok.AllArgsConstructor;

import com.ucm.DentalManagement.model.Treatments;

import java.net.URI;

import javax.validation.Valid;

@RestController
@AllArgsConstructor
public class TreatmentsCrudController {
	@Autowired
	private TreatmentsServices service;
	//private TreatmentsCrud treatmentCrud;
	
	@RequestMapping(path="/listTreatments", method= RequestMethod.GET)
	public ResponseEntity<TreatmentsResponse> getAvailableTreatments(){
		return ResponseEntity.ok(service.getAllTreatments());
	}
	
	@RequestMapping(path="/newTreatment", method = RequestMethod.GET)
	public String newTreatment(ModelMap map) {
		map.put("treatment", new Treatments());
		return "newTreatment";
	}
	
	@RequestMapping(path="/createTreatment", method = RequestMethod.POST)
	public ResponseEntity<UniqueTreatmentsResponse> createTreatment(@Valid @RequestBody NewTreatmentsRequest treatmentRequest){
        UniqueTreatmentsResponse resource = service.createTreatment(treatmentRequest);
        URI uriCreated = UriComponentsBuilder.fromPath("/" + "user-id").buildAndExpand(treatmentRequest.getTreatmentDescription()).toUri();
        return ResponseEntity.created(uriCreated).body(resource);
	}
	
	@RequestMapping(path="/treatmentCreated", method = RequestMethod.POST)
	public String treatmentCreated(@RequestParam("treatment") Treatments treatment) {
		return "treatmentCreated";
	}
	
	/*@RequestMapping(path = "/listTreatments", method = RequestMethod.GET)
	public String getAvailableTreatments(Model map) {
		System.out.println("Obteniendo todos los tratamientos disponibles...");
		map.addAttribute("treatments", treatmentCrud.findAll());
		return "treatmentsList";
	}
	@RequestMapping(path="/newTreatment", method = RequestMethod.GET)
	public String newTreatment(ModelMap map) {
		map.put("treatment", new Treatments());
		return "newTreatment";
	}
	@RequestMapping(path="/createTreatment", method = RequestMethod.POST)
	public String createTreatment(@Valid Treatments treatment, BindingResult bindingResult, ModelMap map) {
		if(bindingResult.hasErrors()) {
			return "redirect:/newTreatment";
		}else {
			treatmentCrud.save(treatment);
			map.put("treatment",treatment);
			return "redirect:/TreatmentCreated";
		}
	}
	@RequestMapping(path="/treatmentCreated", method = RequestMethod.POST)
	public String treatmentCreated(@RequestParam("treatment") Treatments treatment) {
		return "treatmentCreated";
	}*/

}
