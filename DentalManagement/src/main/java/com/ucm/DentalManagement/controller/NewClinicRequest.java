package com.ucm.DentalManagement.controller;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class NewClinicRequest {
	@NotNull
	private int store_id;
	@NotNull
	private String clinicAdress;
}

