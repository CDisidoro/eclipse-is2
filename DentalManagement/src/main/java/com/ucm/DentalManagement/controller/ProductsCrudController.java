package com.ucm.DentalManagement.controller;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.ucm.DentalManagement.model.Products;
import com.ucm.DentalManagement.services.ProductServices;

import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
public class ProductsCrudController {
	@Autowired
	
	private ProductServices service;
	
	@RequestMapping(path="/listProducts", method= RequestMethod.GET)
	public ResponseEntity<ProductsResponse> getAvailableProducts(){
		return ResponseEntity.ok(service.getAllProducts());
	}
	
	@RequestMapping(path="/newProduct", method = RequestMethod.GET)
	public String newProduct(ModelMap map) {
		map.put("product", new Products());
		return "newProduct";
	}
	
	@RequestMapping(path="/createProduct", method = RequestMethod.POST)
	public ResponseEntity<UniqueProductsResponse> createProduct(@Valid @RequestBody NewProductsRequest productRequest){
        UniqueProductsResponse resource = service.createProduct(productRequest);
        URI uriCreated = UriComponentsBuilder.fromPath("/" + "user-id").buildAndExpand(productRequest.getProductName()).toUri();
        return ResponseEntity.created(uriCreated).body(resource);
	}
	
	@RequestMapping(path="/productCreated", method = RequestMethod.POST)
	public String productCreated(@RequestParam("product") Products product) {
		return "productCreated";
	}
	
	//private ProductsCrud productsCrud;
	
	/*@RequestMapping(path = "/listProducts", method = RequestMethod.GET)
	public String getAvailableProducts(Model map) {
		System.out.println("Obteniendo todos los productos disponibles...");
		map.addAttribute("products", productsCrud.findAll());
		return "productsList";
	}
	
	@RequestMapping(path="/newProducts", method = RequestMethod.GET)
	public String newProduct(ModelMap map) {
		map.put("product", new Products());
		return "newProducts";
	}
	@RequestMapping(path="/createProduct", method = RequestMethod.POST)
	public String createProduct(@Valid Products product, BindingResult bindingResult, ModelMap map) {
		if(bindingResult.hasErrors()) {
			return "redirect:/newProducts";
		}else {
			productsCrud.save(product);
			map.put("product",product);
			return "redirect:/ProductCreated";
		}
	}
	@RequestMapping(path="/ProductCreated", method = RequestMethod.POST)
	public String productCreated(@RequestParam("product") Products product) {
		return "ProductCreated";
	}*/
}
