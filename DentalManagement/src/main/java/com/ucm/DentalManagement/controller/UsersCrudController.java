package com.ucm.DentalManagement.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.ucm.DentalManagement.model.Users;
import com.ucm.DentalManagement.services.UsersServicesImpl;

import lombok.AllArgsConstructor;

import java.net.URI;

import javax.validation.Valid;

@RestController
@AllArgsConstructor
public class UsersCrudController {
	@Autowired
	
	private UsersServicesImpl service;
	
	/*@RequestMapping(path = "/listUsers", method = RequestMethod.GET)
	public String getAvailableUsers(Model map) {
		System.out.println("Obteniendo todos los usuarios disponibles...");
		map.addAttribute("users", userCrud.findAll());
		return "usersList";
	}*/
	
	@RequestMapping(path="/listUsers", method= RequestMethod.GET)
	public ResponseEntity<UsersResponse> getAvailableUsers(){
		return ResponseEntity.ok(service.getAllUsers());
	}
	
	@RequestMapping(path="/newUser", method = RequestMethod.GET)
	public String newUser(ModelMap map) {
		map.put("user", new Users());
		return "newUser";
	}
	
	/*@RequestMapping(path="/createUser", method = RequestMethod.POST)
	public String createUser(@Valid Users user, BindingResult bindingResult, ModelMap map) {
		if(bindingResult.hasErrors()) {
			return "redirect:/newUser";
		}else {
			userCrud.save(user);
			map.put("user", user);
			return "redirect:/userCreated";
		}
	}*/
	
	@RequestMapping(path="/createUser", method = RequestMethod.POST)
	public ResponseEntity<UniqueUsersResponse> createUser(@Valid @RequestBody NewUsersRequest userRequest){
        UniqueUsersResponse resource = service.createUser(userRequest);
        URI uriCreated = UriComponentsBuilder.fromPath("/" + "user-id").buildAndExpand(userRequest.getUserName()).toUri();
        return ResponseEntity.created(uriCreated).body(resource);
	}
	
	@RequestMapping(path="/userCreated", method = RequestMethod.POST)
	public String userCreated(@RequestParam("user") Users user) {
		return "userCreated";
	}
}
