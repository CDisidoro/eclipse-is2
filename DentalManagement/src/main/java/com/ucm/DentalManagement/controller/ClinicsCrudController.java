package com.ucm.DentalManagement.controller;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.ucm.DentalManagement.model.Clinics;
import com.ucm.DentalManagement.services.ClinicsServicesImpl;

import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
public class ClinicsCrudController {
	@Autowired
	
	private ClinicsServicesImpl service;
	
	
	@RequestMapping(path="/listClinics", method= RequestMethod.GET)
	public ResponseEntity<ClinicsResponse> getAvailableClinics(){
		return ResponseEntity.ok(service.getAllClinics());
	}
	
	@RequestMapping(path="/newClinic", method = RequestMethod.GET)
	public String newClinic(ModelMap map) {
		map.put("clinic", new Clinics());
		return "newClinic";
	}
	
	@RequestMapping(path="/createClinic", method = RequestMethod.POST)
	public ResponseEntity<UniqueClinicsResponse> createClinic(@Valid @RequestBody NewClinicRequest clinicRequest){
        UniqueClinicsResponse resource = service.createClinics(clinicRequest);
        URI uriCreated = UriComponentsBuilder.fromPath("/" + "user-id").buildAndExpand(clinicRequest.getStore_id()).toUri();
        return ResponseEntity.created(uriCreated).body(resource);
	}
	
	@RequestMapping(path="/clinicCreated", method = RequestMethod.POST)
	public String clinicCreated(@RequestParam("clinic") Clinics clinic) {
		return "clinicCreated";
	}
	
	/*@RequestMapping(path = "/listClinics", method = RequestMethod.GET)
	public String getAvailableClinics(Model map) {
		System.out.println("Obteniendo todos las clinicas disponibles...");
		map.addAttribute("clinics", clinicsCrud.findAll());
		return "clinicsList";
	}
	@RequestMapping(path="/newClinic", method = RequestMethod.GET)
	public String newClinic(ModelMap map) {
		map.put("clinic", new Clinics());
		return "newClinic";
	}
	@RequestMapping(path="/createClinic", method = RequestMethod.POST)
	public String createClinic(@Valid Clinics clinic, BindingResult bindingResult, ModelMap map) {
		if(bindingResult.hasErrors()) {
			return "redirect:/newTreatment";
		}else {
			clinicsCrud.save(clinic);
			map.put("clinic",clinic);
			return "redirect:/ClinicCreated";
		}
	}
	@RequestMapping(path="/clinicCreated", method = RequestMethod.POST)
	public String clinicCreated(@RequestParam("clinic") Clinics treatment) {
		return "clinicCreated";
	}*/
}
