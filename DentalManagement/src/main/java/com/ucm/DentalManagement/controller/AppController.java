package com.ucm.DentalManagement.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ucm.DentalManagement.model.Clients;
import com.ucm.DentalManagement.model.Users;
import com.ucm.DentalManagement.model.UsersCrud;
 
@Controller
public class AppController {
 
    @Autowired
    private UsersCrud userRepo;
     
    /*@GetMapping("")
    public String viewHomePage() {
        return "home";
    }*/
    
    
    @RequestMapping(path = "/register", method = RequestMethod.GET) 
    public String showRegistrationForm(ModelMap map) {
       // model.addAttribute("user", new Users());
         map.put("user", new Users());
        return "register";
    }
    
  
    
   @PostMapping("/process_register")
    public String processRegister(Users user) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encodedPassword = passwordEncoder.encode(user.getUserPassword());
        user.setUserPassword(encodedPassword);
         
        userRepo.save(user);
         
        return "register_success";
    }
   
   @GetMapping("/home")
   public String processLogin(Users user) {
       return "home";
   }
    
    
}
