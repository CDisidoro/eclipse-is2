package com.ucm.DentalManagement.controller;

import lombok.Data;

@Data
public class UniqueUsersResponse {
	private int user_id;
	private String userName;
	private String user_email;
	private String user_password;
	private int user_telephone;
	private String user_type;
	private int user_available;
}
