package com.ucm.DentalManagement.controller;

import lombok.Data;

@Data
public class UniqueClinicsResponse {
	private int store_id;
	private String clinicAdress;
}

