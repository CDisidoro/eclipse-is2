package com.ucm.DentalManagement.controller;

import java.sql.Date;

import javax.validation.constraints.NotNull;

import lombok.Data;


@Data
public class NewProductsRequest {
	@NotNull
	private int product_id;	
	@NotNull
	private String productName;
	@NotNull
	private Integer stock_num;
	@NotNull
	private Date day_of_arrival;
	@NotNull
	private int store_id;	
}
