package com.ucm.DentalManagement.controller;

import java.sql.Date;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class NewTreatmentsRequest {
	@NotNull
	private int treatment_id;
	@NotNull
	private String client_dni;
	@NotNull
	private String treatmentDescription;
	@NotNull
	private int treatment_price;
	@NotNull
	private Date treatment_start_date;
	@NotNull
	private Date treatment_end_date;
}
