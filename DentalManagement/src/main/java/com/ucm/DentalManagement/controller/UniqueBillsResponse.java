package com.ucm.DentalManagement.controller;

import java.sql.Date;
import lombok.Data;

@Data
public class UniqueBillsResponse {
	private int bill_id;
	private String clientDni;
	private int bill_price;
	private Date bill_expedition_date;
	private String bill_payed;
}
