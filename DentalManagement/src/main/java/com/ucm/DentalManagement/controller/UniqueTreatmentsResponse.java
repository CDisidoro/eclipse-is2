package com.ucm.DentalManagement.controller;

import java.sql.Date;
import lombok.Data;

@Data
public class UniqueTreatmentsResponse {
	private int treatment_id;
	private String client_dni;
	private String treatmentDescription;
	private int treatment_price;
	private Date treatment_start_date;
	private Date treatment_end_date;
}
