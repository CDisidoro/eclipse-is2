package com.ucm.DentalManagement.controller;

import java.sql.Date;

import lombok.Data;

@Data
public class UniqueProductsResponse {
	private int product_id;
	private int store_id;
	private String productName;
	private Date day_of_arrival;
	private Integer stock_num;
}
