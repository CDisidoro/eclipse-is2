package com.ucm.DentalManagement.controller;

import java.util.List;

import lombok.Data;

@Data
public class UsersResponse {
	private List<UniqueUsersResponse> users;
}
