package com.ucm.DentalManagement.controller;

import java.sql.Date;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class NewBillsRequest {
	@NotNull
	private int bill_id;
	@NotNull
	private String clientDni;
	@NotNull
	private int bill_price;
	@NotNull
	private Date bill_expedition_date;
	@NotNull
	private String bill_payed;
}
