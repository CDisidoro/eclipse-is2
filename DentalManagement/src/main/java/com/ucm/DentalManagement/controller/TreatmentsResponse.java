package com.ucm.DentalManagement.controller;

import java.util.List;

import lombok.Data;

@Data
public class TreatmentsResponse {
	private List<UniqueTreatmentsResponse> treatments;
}
