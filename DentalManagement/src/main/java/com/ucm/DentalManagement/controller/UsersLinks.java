package com.ucm.DentalManagement.controller;

public class UsersLinks {
	static final String USERS_DEFAULT_URI="/users/";
	static final String SINGLE_USER_URI=USERS_DEFAULT_URI + "{user-id}";
	
	private UsersLinks(){}
}
